import React from 'react';
import {BrowserRouter, BrowserRouter  as Router, Link, Route, Routes} from "react-router-dom"
import './App.css';
import Navbar from './Components/Navbar/Navbar';
import MissionsBoard from './Components/Missions/MissionBoard';
import AboutUs from './Components/AboutUs/AboutUs';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/* <Navbar /> */}

      <BrowserRouter>
        <div className="header">
          <Navbar />
        </div>
        <Routes>
            <Route path='/' element={<h1>basic</h1>} />
            <Route path="missions" element={<MissionsBoard />} />
            <Route path="aboutUs" element={<AboutUs />} />
            <Route path="*" element={<h5>nothing</h5>} />
        </Routes>
      </BrowserRouter>
      </header>
    </div>
  );
}

export default App;
