export interface MissionModel{
    name: string,
    needToKnow: string[]
    body: string,
    server: string,
    port: number,
    id: number
}

export interface BackendMissionModel{
    subject: string,
    name: string,
    id: number,
    needToKnow: string[],
    body: string,
    server: string,
    port: number
}

export const ToMissionModel = (backendMissionModel: BackendMissionModel) => {
    return {
        name: backendMissionModel.name,
        needToKnow: backendMissionModel.needToKnow,
        id: backendMissionModel.id,
        body: backendMissionModel.body,
        server: backendMissionModel.server,
        port: backendMissionModel.port
    }
}

export interface MissionRowModel{
    title: string,
    missions:MissionModel[]
}