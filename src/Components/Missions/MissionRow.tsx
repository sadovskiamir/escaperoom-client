import React from 'react';
import { MissionModel, MissionRowModel } from '../../Models/MissionsModel';
import Mission from './Mission';
import './Missions.scss'

interface MissionRowProps{
    missions : MissionRowModel
}

const MissionRow = (props : MissionRowProps) => {
    return (
        <div className="missions-row">
            <h1 className="missions-title">
                {props.missions.title}
            </h1>
            <hr />
            <div className="missions">
                {props.missions.missions.map((mission) => <Mission {...mission}/>)}
            </div>
        </div>
    )
}

export default MissionRow;