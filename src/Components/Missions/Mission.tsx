import { Card, CardActions, CardContent, IconButton, InputAdornment, TextField, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { MissionModel } from '../../Models/MissionsModel';
import SendIcon from '@mui/icons-material/Send';
import './Missions.scss'
import { borderColor } from '@mui/system';

const Mission = (props : MissionModel) => {
    const [answer, setAnswer] = useState('')
    const [missionComplete, setMissionComplete] = useState(false)

    useEffect(() => {
        fetch(`http://localhost:8080/challenge/Completed/amir/${props.id}`)
            .then(response => response.json())
            .then(data => setMissionComplete(data))
    }, [])


    const HandleSend = (e: any) => {
        fetch("", e.target.value)
    }
    return (
        <div className={`mission ${missionComplete ? 'mission-completed' : ''}`}>

            <h1>{props.name}</h1>
            <p className="body">
                {/* {props.body} */}
                Need to do 
                <h3>{props.needToKnow.join(', ')}</h3>
            </p>
            {/* <TextField className="answer-field" label="Answer" variant="filled" color='primary' focused InputProps={{
                endAdornment: (
                // <IconButton onclick={HandleSend}>
                <IconButton>
                    <SendIcon />
                </IconButton>
                )
                }}
            /> */}
        </div>
    )
}

export default Mission;