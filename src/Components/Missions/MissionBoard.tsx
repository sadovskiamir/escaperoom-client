import React, { useEffect, useState } from 'react';
import {BackendMissionModel, MissionModel, MissionRowModel, ToMissionModel} from '../../Models/MissionsModel'
import MissionRow from './MissionRow';
import './Missions.scss'


const MissionsBoard = () => {

    const [missions, setMissions] = useState<BackendMissionModel[]>([]);
    const [filteredMissionsRows, setFilteredMissionsRows] = useState<MissionRowModel[]>([]);
    const [searchText, setSearchText] = useState<string>("");

    useEffect(() => {
        fetch('http://localhost:8080/challenge')
            .then(response => response.json())
            .then(data => setMissions(data));
        
    }, [])
    
    useEffect(() => {
        setFilteredMissionsRows(toRowMissions(missionsGroupBy(missions.filter((m) => m.name.includes(searchText)))));
        
    }, [searchText, missions])
    
    const missionsGroupBy = (missions: BackendMissionModel[]) => {
        return missions.reduce((curentMissions: any, mission: any) => {
            curentMissions[mission.subject] = [...curentMissions[mission.subject] || [], mission];
            return curentMissions;
           }, {});
    }

    const toRowMissions = (missions : any) => {
        const rowMissions : MissionRowModel[] = [];
        
        Object.entries(missions).forEach(
            ([key, value]) => rowMissions.push(
                {
                    title: key,
                    missions: (value as BackendMissionModel[]).map(m => ToMissionModel(m))
                })
          );
        
        return rowMissions;
    }

    const handleFilterChange = (e: any) => {
        setSearchText(e.target.value);
    }

    return (
        <div className="missions-board">
            <div className="search-field">
                <input type="text" onChange={handleFilterChange} name="name" placeholder="Filter.." />
            </div>

            {/* <TextField className="filter-field" label="Filter" variant="filled" onChange={handleFilterChange} focused InputProps={{
                endAdornment: (
                // <IconButton onclick={HandleSend}>
                <IconButton>
                    <SendIcon />
                </IconButton>
                )
                }}
            /> */}
            {
                filteredMissionsRows.map((missionsRow) => 
                    <MissionRow missions= {missionsRow}/>)
            }
        </div>
    );
}

export default MissionsBoard;