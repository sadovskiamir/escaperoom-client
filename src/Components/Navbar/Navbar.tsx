import {Link} from "react-router-dom"
import User from "./User";
import logo from '../../hacker.png'

import './Navbar.scss'

const Navbar = () => {
    return (
        <div className="navbar">
            <div className="navbar-routes">
                <Link className="logo" to="/">
                <img className="logo-image" src={logo} alt="logo" />
                <h5>Herzcape</h5>
                </Link>
                <Link to="/missions"></Link>
                <Link to="/aboutUs">About us</Link>
                <Link to="/missions">missions</Link>
                <User />
            </div>
        </div>
    )
}

export default Navbar;